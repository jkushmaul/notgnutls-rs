use gnutls_rs::certificates::{Credentials, CredentialsBuilder, GnutlsSecParams};
use gnutls_rs::cookie_key::CookieKey;
use gnutls_rs::{dtls, Settings};
use std::io::{Error, ErrorKind, Result};
use std::net::SocketAddr;
use std::path::PathBuf;
use std::pin::Pin;
use std::time::Duration;
use tokio::io::{AsyncBufReadExt, BufReader};
use tokio::net::UdpSocket;
use tokio::runtime::Handle;
use tokio::time::sleep;
use udp_listener::UdpStack;
use gnutls_rs::dtls::{DtlsStream, Transport};
use gnutls_sys::gnutls_bindings::{GNUTLS_E_AGAIN, GNUTLS_E_INTERRUPTED, gnutls_global_init};


fn parse_addr(addr: &str) -> Result<SocketAddr> {
    let s: SocketAddr = match addr.parse() {
        Ok(s) => s,
        Err(e) => return Err(Error::new(ErrorKind::AddrNotAvailable, e))
    };
    return Ok(s);
}

#[tokio::main]
pub async fn main() {
    env_logger::init();


    unsafe {
        gnutls_global_init();
    }

    gnutls_rs::log::set_log_level(31337);
    gnutls_rs::log::set_log_func(|s| {
        log::trace!("GNUTLS - {}", s);
    });

    let client_addr: &str = "127.0.0.1:31338";
    let server_addr: &str = "127.0.0.1:31337";

    //in this example - the server, and the client are never meant to usually be running in the same runtime
    //so I am making 2 threads to troubleshoot a possible blocking issue.
    run_server(server_addr).await;


    println!("Main is done.");
}


/*
        13. loop {
        13.1 gnutls_record_recv_seq(ciphertext)
        13.1.a if < 0 continue
        13.1.b if == 0; EOF; break
        13.1.c if > 0; plaintext from recv_seq.
        13.2 gnutls_record_send(plaintext)
        13.2.a if < 0; break;
        }
        14. gnutls bye
        15. deinit session
*/
async fn run_server(
    server_addr: &str,
) -> Result<()> {
    log::debug!("run_server");

    let handle = Handle::current();
    let mtu = 1400;

    log::debug!("run_server - bind to {}", server_addr);
    let socket = UdpSocket::bind(server_addr).await?;

    log::debug!("run_server - starting a new stack");
    let stack = UdpStack::new("server", socket, mtu, 4096);
    log::debug!("run_server - stack listener");
    let mut listener = stack.listen(handle, 64).await?;

    log::debug!("************************************");
    log::debug!("::listen_stream() - waiting for a stream");
    let udp_stream = match listener.accept().await {
        Ok(s) => s,
        Err(e) => {
            log::debug!("listen_stream() - got error in accept: {}", e);
            return Err(e);
        }
    };

    let settings = build_settings("server");

    log::debug!("run_server - listening for dtls stream");
    let result = dtls::listen_stream(settings, udp_stream).await;
    let mut stream = match result {
        Ok(stream) => stream,
        Err(e) => return Err(e)
    };

    log::debug!("************************************");
    log::debug!("run-server - Got stream");
    loop {
        match stream.recv() {
            Ok(decrypted) => {
                if let Ok(str) = String::from_utf8(decrypted) {
                    log::debug!("Received: {}", str);
                    let clear_text_to_send = format!("You said: {}", str);
                    if let Ok(_) = stream.send(&clear_text_to_send.as_bytes()) {
                        log::debug!("sent: {}", clear_text_to_send);
                    }
                }
            },
            Err(e) => {
                let e = e as i32;
                if e == GNUTLS_E_AGAIN || e == GNUTLS_E_INTERRUPTED {
                    continue;
                }
                log::error!("Error from gnutls stream: {}", e);
                break;
            }
        }
    }
    listener.stop().await;
    Ok(())
}

fn build_settings(_: &str) -> Settings {
    //../x509-ca-key.pem  ../x509-ca.pem  ../x509-client.p12  ../x509-server-key.pem  ../x509-server.pem
    let ca_file = "target/ca-cert.pem";
    let cert_file = "target/cert.pem";
    let key_file = "target/key.pem";
    let x509: Credentials = CredentialsBuilder::new()
        .ca_file(PathBuf::from(ca_file))
        //.crl_file(PathBuf::from(crl_file))
        .key_files(PathBuf::from(key_file), PathBuf::from(cert_file))
        .sec_params(GnutlsSecParams::GnutlsSecParamMedium)
        .build()
        .expect("Could not create credentials");


    let mtu = 1400;

    //generate cookie key
    let cookie: CookieKey = CookieKey::generate().expect("Could not generate cookie");

    gnutls_rs::Settings::new(x509,  cookie, mtu)
}
