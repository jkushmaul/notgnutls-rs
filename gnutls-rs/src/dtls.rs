use crate::session::{Session, SessionBuilder};
use crate::{CookieKey, Settings};
use core::mem;
use gnutls_sys::gnutls_bindings::{gnutls_dtls_prestate_st, GNUTLS_E_AGAIN, GNUTLS_E_INTERRUPTED, GNUTLS_E_SESSION_EOF, GNUTLS_E_SHORT_MEMORY_BUFFER, gnutls_init_flags_t, gnutls_record_recv, gnutls_record_send, gnutls_transport_ptr_t, set_errno};
use std::io::{Error, ErrorKind};
use std::time::Duration;
use tokio::time::sleep;
use std::io::Result;
use std::ops::DerefMut;
use std::os::raw::c_void;
use std::pin::Pin;
use std::slice;
use libc::EAGAIN;
use tokio::sync::mpsc::error::{TrySendError};
use udp_listener::{UdpDatagram, UdpStream};


pub struct DtlsPrestate {
    pub(crate) prestate: gnutls_dtls_prestate_st,
}

impl DtlsPrestate {
    pub(crate) fn new() -> Self {
        unsafe {
            let prestate = mem::zeroed();
            return Self { prestate };
        }
    }
}

pub async fn connect_session(
    settings: Settings,
    transport: Transport,
) -> std::io::Result<Option<Session>> {
    let mut builder = SessionBuilder::new("client");
    builder.flags(gnutls_init_flags_t::GNUTLS_DATAGRAM | gnutls_init_flags_t::GNUTLS_NONBLOCK  | gnutls_init_flags_t::GNUTLS_CLIENT )
        .credentials(&settings.x509)
        .dtls_mtu(settings.mtu);

    let mut session = builder
        //.dtls_timeouts(1000, 60000)
        .build(transport)
        .expect("Could not create DTLS Session");





    log::debug!("dtls::connect_stream - starting handshake");
    //at least prestate is acceptable.  But we still need to handshake session.
    loop {
        return match session.handshake() {
            Ok(_) => {
                log::debug!("Made a handshake!");
                Ok(Some(session))
            }
            Err(ret) => {
                if ret == GNUTLS_E_AGAIN {
                    continue;
                }
                log::debug!("connect_stream - handshake error: {}", ret);
                Err(Error::new(ErrorKind::Other, "handshake failed"))
            }
        }
    }

}


/* TLS record content is the first by of the packet */
const RECORD_CONTENT_POS: usize = 0;
/* handshake type is first byte in Handshake packet;
 * we have to skip type;version;epoch;sequence_number;
 * and length in DTLSPlaintext */
const HANDSHAKE_TYPE_POS: usize = 13;
/* record layer indication for a handshake packet */
const HANDSHAKE_CONTENT_TYPE: u8 = 22;
const GNUTLS_HANDSHAKE_CLIENT_HELLO: u8 = 1;

fn is_valid_handshake(dg: &UdpDatagram) -> bool {
    return dg.bytes.len() > HANDSHAKE_TYPE_POS &&
        dg.bytes[RECORD_CONTENT_POS] == HANDSHAKE_CONTENT_TYPE &&
        dg.bytes[HANDSHAKE_TYPE_POS] == GNUTLS_HANDSHAKE_CLIENT_HELLO;
}
async fn establish_prestate(mut cookie_key: CookieKey, udp_stream: &mut UdpStream) -> Result<DtlsPrestate> {
    loop {
        log::debug!("establish_prestate: peeking");
        let dg = match udp_stream.peek().await {
            Ok(Some(s)) => s,
            Ok(None) => continue,
            Err(e) => return Err(e)
        };
        log::debug!("establish_prestate: received {} from {}", dg.bytes.len(), dg.sa_addr);
        let valid_handshake = is_valid_handshake(&dg);
        if valid_handshake {
            match cookie_key.verify(&dg.sa_addr, &dg.bytes) {
                Ok(Some(s)) => {
                    log::debug!("establish_prestate: established");
                    return Ok(s);
                },
                Ok(None) => {}
                Err(e) => {
                    log::debug!("There was an error verifying prestate");
                    return Err(e)
                },
            };
        }
        log::debug!("discarding peek'd message");
        let _discard = udp_stream.try_recv()?;
        if !valid_handshake {
            log::debug!("was not a valid handshake");
            continue;
        }

        log::debug!("generating prestate");
        let hvr = match cookie_key.send(udp_stream.get_addr()) {
            Ok(v) => v,
            Err(e) => {
                log::debug!("Error generating prestate: {}", e);
                return Err(Error::new(ErrorKind::InvalidData, e.to_string()));
            }
        };

        log::debug!("sending prestate of len {} bytes", hvr.len());
        udp_stream.send(&hvr).await?;

        log::debug!("sleeping 100us");
        sleep(Duration::from_micros(100)).await;
    }
}

pub async fn listen_stream(
    settings: Settings,
    mut udp_stream: UdpStream,
) -> std::io::Result<DtlsStream> {
    log::debug!("************************************");
    log::debug!("::listen_stream() - establishing_prestate");
    let cookie_key = settings.cookie_key;
    let mtu = settings.mtu;
    let x509 = settings.x509;
    let prestate = match establish_prestate(cookie_key, &mut udp_stream).await {
        Ok(s) => s,
        Err(e) => {
            log::debug!("listen_stream() - got error in establish_prestate: {}", e);
            return Err(e);
        }
    };

    log::debug!("************************************");
    log::debug!("::listen_stream() - starting session");
    let transport = Transport::new("server", mtu, udp_stream);
    let mut builder = SessionBuilder::new("server");
    builder
        .flags(gnutls_init_flags_t::GNUTLS_DATAGRAM  | gnutls_init_flags_t::GNUTLS_NONBLOCK |  gnutls_init_flags_t::GNUTLS_SERVER)
        .credentials(&x509)
        .dtls_mtu(mtu)
        //.dtls_timeouts(1000, 60000)
        .priority("NORMAL")
        .dtls_prestate(prestate);
    let mut session = builder
        .build(transport)
        .expect("Could not create DTLS Session");


    log::debug!("************************************");
    log::debug!("Attempting to establish handshake");
    loop {
        match session.handshake() {
            Ok(_) => {
                log::debug!("************************************");
                log::debug!("listen_stream - handshake complete");
                let dtls_stream = DtlsStream::new(session);
                return Ok(dtls_stream);
            }
            Err(e) => {
                if e != GNUTLS_E_INTERRUPTED && e != GNUTLS_E_AGAIN {
                    log::error!("listen_stream - handshake error: {}", e);
                    return Err(Error::new(ErrorKind::Other, format!("listen_stream - handshake error: {}", e)));
                }
            }
        }
       // tokio::time::sleep(Duration::from_millis(100)).await;
    }
}

pub struct DtlsStream {
    session: Session,
    mtu: usize,
}

impl DtlsStream {

    pub fn new(session: Session) -> Self {

        /*
      8. set transport  ptr
      9. set transport push function
      10. set transport pull
      11. set pull timeout
       */
        let mtu = session.get_transport().mtu;

        let dtls_stream = Self {
            session,
            mtu,
        };



        return dtls_stream;
    }

    pub fn send(&mut self, data: &[u8]) -> std::result::Result<isize, isize> {
        // gnutls_record_send *could, potentially block... I think it would be better to simulate a stack
        // and use message passing here instead, similar to UdpStack

        //for now...

        unsafe {
            //this will call push func
            let ret = gnutls_record_send(
                *self.session.get_session(),
                data.as_ptr() as *mut c_void,
                data.len(),
            );
            return if ret < 0 { Err(ret) } else { Ok(ret) };
        }
    }

    pub fn recv(&mut self) -> std::result::Result<Vec<u8>, isize> {
        //instead, this should recv from channel, the stack will forward it through.
        unsafe {
            let mut read_buffer: Vec<u8> = vec![0u8; self.mtu];
            let data = read_buffer.as_mut_ptr() ;
            //this will call pull func
            let ret = gnutls_record_recv(
                *self.session.get_session(),
                data as *mut c_void,
                read_buffer.capacity(),
            );

            if ret < 0 {
                return Err(ret)
            } ;
            if ret as usize > read_buffer.capacity() {
                return Err(GNUTLS_E_SHORT_MEMORY_BUFFER as isize);
            }
            read_buffer.set_len(ret as usize);
            Ok(read_buffer)
        }
    }
    pub async fn shutdown(mut self) -> std::io::Result<()> {
        println!("DtlsStream::shutdown()");
        self.session.bye();
        Ok(())
    }
}


#[repr(C)]
#[derive(Debug)]
pub struct Transport {
    name: String,
    mtu: usize,
    stream:  Pin<Box<UdpStream>>,
}

impl Transport {
    pub fn new(name: &str, mtu: usize, stream: UdpStream) -> Self {
        Self { name: name.to_string(), mtu, stream: Pin::new(Box::new(stream)) }
    }
    pub fn get_mtu(&self) -> usize {
        return self.mtu;
    }
    pub fn to_cvoid(mut self: &mut Pin<Box<Self>>) -> *mut c_void {
        return self.deref_mut() as *mut _ as *mut c_void;
    }

    fn from_cvoid(tx_ptr: *mut c_void) -> *mut Transport {
        return tx_ptr as *mut Transport;
    }


    #[no_mangle]
    pub(crate) extern "C" fn udp_stream_pull_timeout_func(
        tx_ptr: gnutls_transport_ptr_t,
        _ms: ::std::os::raw::c_uint,
    ) -> ::std::os::raw::c_int {
        unsafe {
            let transport = Transport::from_cvoid(tx_ptr);

            match (*transport).stream.try_peek() {
                Ok(Some(_)) => {
                    return 1
                }
                Ok(None) => {
                    return 0
                }
                Err(_) => {
                    return -1
                }
            }
        }
    }

    #[no_mangle]
    pub(crate) extern "C" fn udp_stream_pull_func(
        tx_ptr: gnutls_transport_ptr_t,
        data: *mut ::std::os::raw::c_void,
        data_len: usize,
    ) -> isize {
        unsafe {
            let transport = Transport::from_cvoid(tx_ptr);
            let packet = match (*transport).stream.try_recv() {
                Ok(Some(s)) => {
                    log::debug!("udp_stream_pull_func - {} - try_recv has data",  (*transport).name);
                    s
                },
                Ok(None) => {
                    set_errno(EAGAIN);
                    return -1;
                },
                Err(_) => {
                    log::debug!("udp_stream_pull_func - {} - try_recv was disconnected", (*transport).name);
                    return -1;
                },
            };
            if packet.bytes.len() > data_len {
                log::debug!("udp_stream_pull_func - {} - buffer was not large enough for packet", (*transport).name);
                return -1;
            }


            let buffer = slice::from_raw_parts_mut(data as *mut u8, packet.bytes.len());
            buffer.copy_from_slice(&packet.bytes);
            log::debug!("udp_stream_pull_func - {} - copied {} bytes into {} bytes",  (*transport).name, packet.bytes.len(), buffer.len());
            return buffer.len() as isize;
        }
    }

    #[no_mangle]
    pub(crate) extern "C" fn udp_stream_push_func(
        tx_ptr: gnutls_transport_ptr_t,
        hvr: *const ::std::os::raw::c_void,
        hvr_len: usize,
    ) -> isize {
        unsafe {
            let transport = Transport::from_cvoid(tx_ptr);
            log::debug!("udp_stream_push_func - {} - data_len: {}", (*transport).name, hvr_len);
            let hvr: *const u8 = hvr as *const u8;
            let hvr: &[u8] = slice::from_raw_parts(hvr, hvr_len);
            //copy it so it does not get free'd
            let mut packet = vec![0u8; hvr.len()];
            packet.copy_from_slice(hvr);
            let pushresult = match (*transport).stream.try_send(&packet) {
                Ok(_) => packet.len() as isize,
                Err(TrySendError::Full(_)) => GNUTLS_E_AGAIN as isize,
                Err(TrySendError::Closed(_)) => GNUTLS_E_SESSION_EOF as isize,
            };

            log::debug!("udp_stream_push_func - {} - try_send() result was {}", (*transport).name, pushresult);
            return pushresult;
        }
    }
}