use crate::with_int_result;
use core::mem;
use std::ffi::CString;
use gnutls_sys::gnutls_bindings::gnutls_sec_param_t::{
    GNUTLS_SEC_PARAM_EXPORT, GNUTLS_SEC_PARAM_FUTURE, GNUTLS_SEC_PARAM_HIGH,
    GNUTLS_SEC_PARAM_INSECURE, GNUTLS_SEC_PARAM_LEGACY, GNUTLS_SEC_PARAM_LOW, GNUTLS_SEC_PARAM_MAX,
    GNUTLS_SEC_PARAM_MEDIUM, GNUTLS_SEC_PARAM_ULTRA, GNUTLS_SEC_PARAM_UNKNOWN,
    GNUTLS_SEC_PARAM_VERY_WEAK, GNUTLS_SEC_PARAM_WEAK,
};
use gnutls_sys::gnutls_bindings::gnutls_x509_crt_fmt_t::GNUTLS_X509_FMT_PEM;
use gnutls_sys::gnutls_bindings::{
    gnutls_certificate_allocate_credentials, gnutls_certificate_free_credentials,
    gnutls_certificate_set_known_dh_params, gnutls_certificate_set_x509_crl_file,
    gnutls_certificate_set_x509_key_file, gnutls_certificate_set_x509_system_trust,
    gnutls_certificate_set_x509_trust_file,
};
use gnutls_sys::gnutls_bindings::{gnutls_certificate_credentials_t};
use std::path::PathBuf;

struct KeyFiles {
    cert_file: PathBuf,
    key_file: PathBuf,
}
pub struct CredentialsBuilder {
    ca_file: Option<PathBuf>,
    crl_file: Option<PathBuf>,
    key_files: Option<KeyFiles>,
    sec_params: Option<GnutlsSecParams>,
}
impl CredentialsBuilder {
    pub fn new() -> Self {
        return Self {
            ca_file: None,
            crl_file: None,
            key_files: None,
            sec_params: None,
        };
    }
    pub fn ca_file(mut self, ca_file: PathBuf) -> Self {
        self.ca_file = Some(ca_file);
        return self;
    }
    pub fn crl_file(mut self, crl_file: PathBuf) -> Self {
        self.crl_file = Some(crl_file);
        return self;
    }
    pub fn key_files(mut self, key_file: PathBuf, cert_file: PathBuf) -> Self {
        self.key_files = Some(KeyFiles {
            key_file,
            cert_file,
        });
        return self;
    }
    pub fn sec_params(mut self, sec_params: GnutlsSecParams) -> Self {
        self.sec_params = Some(sec_params);
        return self;
    }
    pub fn build(self) -> Result<Credentials, i32> {
        let mut c: Credentials = Credentials::new();
        c.init().unwrap();

        if let Some(ca_file) = self.ca_file {
            c.set_ca_trust_file(&ca_file).expect(format!("Could not set trust file: {}", ca_file.display()).as_str());
        }
        if let Some(crl_file) = self.crl_file {
            c.set_crl_file(&crl_file).expect(format!("Could not set crl file: {}", crl_file.display()).as_str());
        }
        if let Some(key_files) = self.key_files {
            c.set_key_file(&key_files.cert_file, &key_files.key_file)
                .expect(format!("Could not set cert/key files: {} and {}", key_files.cert_file.display(), key_files.key_file.display()).as_str());
        }
        if let Some(params) = self.sec_params {
            c.set_known_dh_params(params.value())?;
        }

        return Ok(c);
    }
}
#[derive(Debug, Clone)]
pub struct Credentials {
    xcred: gnutls_certificate_credentials_t,
}

unsafe impl Sync for Credentials {}
unsafe impl Send for Credentials {}

impl Credentials {
    fn new() -> Self {
        unsafe {
            let x: gnutls_certificate_credentials_t = mem::zeroed();
            return Self { xcred: x };
        }
    }
    fn init(&mut self) -> Result<i32, i32> {
        return with_int_result(|| unsafe {
            return gnutls_certificate_allocate_credentials(&mut self.xcred);
        });
    }
    pub(crate) fn get_xcred(&self) -> gnutls_certificate_credentials_t {
        return self.xcred;
    }

    #[allow(dead_code)]
    fn set_system_trust(&self) -> Result<i32, i32> {
        return with_int_result(|| unsafe {
            return gnutls_certificate_set_x509_system_trust(self.xcred);
        });
    }
    fn set_crl_file(&self, file_path: &PathBuf) -> Result<i32, i32> {
        let crlfile = CString::new(file_path.to_str().unwrap()).unwrap();

        return with_int_result(|| unsafe {
            let result = gnutls_certificate_set_x509_crl_file(
                self.xcred,
                crlfile.as_ptr(),
                GNUTLS_X509_FMT_PEM,
            );
            return if result > 0 {
                0
            } else {
                -1
            }
        });
    }

    fn set_ca_trust_file(&self, file_path: &PathBuf) -> Result<i32, i32> {
        let cafile =  CString::new(file_path.to_str().unwrap()).unwrap();
        return with_int_result(|| unsafe {
            let result = gnutls_certificate_set_x509_trust_file(
                self.xcred,
                cafile.as_ptr(),
                GNUTLS_X509_FMT_PEM,
            );
            return if result > 0 {
                0
            } else {
                -1
            }
        });
    }

    fn set_key_file(&self, cert_file_path: &PathBuf, key_file_path: &PathBuf) -> Result<i32, i32> {
        let cert_file_path_str =  CString::new(cert_file_path.to_str().unwrap()).unwrap();
        let key_file_path_str = CString::new(key_file_path.to_str().unwrap()).unwrap();
        return with_int_result(|| unsafe {
            return gnutls_certificate_set_x509_key_file(
                self.xcred,
                cert_file_path_str.as_ptr(),
                key_file_path_str.as_ptr(),
                GNUTLS_X509_FMT_PEM,
            );
        });
    }

    fn set_known_dh_params(&self, sec_params: u32) -> Result<i32, i32> {
        return with_int_result(|| unsafe {
            return gnutls_certificate_set_known_dh_params(self.xcred, sec_params);
        });
    }
}

impl Drop for Credentials {
    fn drop(&mut self) {
        unsafe {
            gnutls_certificate_free_credentials(self.xcred);
        }
    }
}

/// Not savvy enough to know why but was warded off from using "rustified_enums" as being dangerous...?
/// So just mapping a few out here that will need to be used from rust.
pub enum GnutlsSecParams {
    GnutlsSecParamUnknown,
    GnutlsSecParamInsecure,
    GnutlsSecParamExport,
    GnutlsSecParamVeryWeak,
    GnutlsSecParamWeak,
    GnutlsSecParamLow,
    GnutlsSecParamLegacy,
    GnutlsSecParamMedium,
    GnutlsSecParamHigh,
    GnutlsSecParamUltra,
    GnutlsSecParamFuture,
    GnutlsSecParamMax,
}

impl GnutlsSecParams {
    pub(crate) fn value(&self) -> u32 {
        match self {
            GnutlsSecParams::GnutlsSecParamUnknown => GNUTLS_SEC_PARAM_UNKNOWN,
            GnutlsSecParams::GnutlsSecParamInsecure => GNUTLS_SEC_PARAM_INSECURE,
            GnutlsSecParams::GnutlsSecParamExport => GNUTLS_SEC_PARAM_EXPORT,
            GnutlsSecParams::GnutlsSecParamVeryWeak => GNUTLS_SEC_PARAM_VERY_WEAK,
            GnutlsSecParams::GnutlsSecParamWeak => GNUTLS_SEC_PARAM_WEAK,
            GnutlsSecParams::GnutlsSecParamLow => GNUTLS_SEC_PARAM_LOW,
            GnutlsSecParams::GnutlsSecParamLegacy => GNUTLS_SEC_PARAM_LEGACY,
            GnutlsSecParams::GnutlsSecParamMedium => GNUTLS_SEC_PARAM_MEDIUM,
            GnutlsSecParams::GnutlsSecParamHigh => GNUTLS_SEC_PARAM_HIGH,
            GnutlsSecParams::GnutlsSecParamUltra => GNUTLS_SEC_PARAM_ULTRA,
            GnutlsSecParams::GnutlsSecParamFuture => GNUTLS_SEC_PARAM_FUTURE,
            GnutlsSecParams::GnutlsSecParamMax => GNUTLS_SEC_PARAM_MAX,
        }
    }
}
