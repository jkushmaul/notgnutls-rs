#include <gnutls/gnutls.h>
#include <gnutls/dtls.h>
#include <string.h>
#include <errno.h>

int get_errno() {
    return errno;
}
void set_errno(int newerrno) {
    errno = newerrno;
}

/*
Because I think the callbacks are a major issue, especially when using this within an async framework
(Try to await, in an FFI callback - and show me an example not some shitty documentation that skirts around it
on async-ff).

From this:
int
gnutls_dtls_cookie_send (gnutls_datum_t *key,
                         void *client_data,
                         size_t client_data_size,
                         gnutls_dtls_prestate_st *prestate,
                         gnutls_transport_ptr_t ptr,
                         gnutls_push_func push_func);

where you have to provide an isize push function


And instead have a function like this:
int gnutls_dtls_cookie_generate(gnutls_datum_t * key,
                                                void * client_data,
                                                 size_t client_data_size,
                                                  gnutls_dtls_prestate_st *prestate,
                                                  gnutls_datum_t *buffer
                                                 ) {

Which simply copies the data into buffer, to be sent.  > 0 success, < 0 error code no buffer.
If > 0, then it's up to the caller to send it.

To accomplish this, I'd hide the callback form of this, so that we simply
copy the data back using our FFI, but then we are able to handle the transport outside of FFI
and may once again use Async.

Now in my very specific use case, this was OK - this is literally all the callback was for - to generate
an error code to return back from the main function... Seemed kind of silly to make this so
complex with callbacks etc...

copy_func(gnutls_transport_ptr_t p, const void *data, size_t size)
{
        gnutls_datum_t *priv = p;
        if (priv == NULL || priv->size < size ) {
            return -1;
        }
        memcpy(priv->data, data, size);
        return size;
}

int gnutls_dtls_cookie_generate(gnutls_datum_t * key,
                                                void * client_data,
                                                 size_t client_data_size,
                                                  gnutls_dtls_prestate_st *prestate,
                                                  gnutls_datum_t *buffer
                                                 ) {
    if (buffer == NULL || buffer->size == 0) {
        return -1;
    }

    return gnutls_dtls_cookie_send(key, client_data, client_data_size, prestate, buffer, copy_func);
}



// For receive - we can already sit in front of that I believe... We push data read, into gnutls ,it does not tell us to read.
// The only thing it does tell us to do, is send - and that is what breaks my async stuff.

// From what I can tell these are the signatures where push func is even used:

* gnutls_dtls_cookie_generate  - it quite literally calls the push func and returns it's result...  lame.


actually this seems to be used ina  manner that might make this not possible due to write vectors + flushing.
/ * This function writes the data that are left in the
 * TLS write buffer (ie. because the previous write was
 * interrupted.
 * /

ssize_t _gnutls_io_write_flush(gnutls_session_t session)
{
   ...
   push_func()


 We'd have to ensure that the data is copied all the way back.


Perhaps what we can do instead, is use messages

//if these were proper rust callbacks

func push_func(p: *MySyncMessageSender, data: const void *, size: size_t) -> ssize_t
{
        (*MySyncMessageSender).Send(data, size_t);
        return size_t;
}

func pull_func(p: *MySyncMessageReceiver, data: const void *, size: size_t) -> ssize_t
{
    if (*MySyncMessageReceiver).hasData() {
       Vec<u8> v = (*MySyncMessageReceiver).getMessage();
       v.copy_to(data);
       size = v.length();
    } else {
        return 0;
    }
}

*/
