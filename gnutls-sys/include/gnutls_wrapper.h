#include <stdint.h>
#include <gnutls/gnutls.h>
#include <gnutls/dtls.h>

// lib/gnutls_int.h
#define DTLS_MAX_COOKIE_SIZE 32
#define TLS_HANDSHAKE_HEADER_SIZE 4
#define DTLS_HANDSHAKE_HEADER_SIZE (TLS_HANDSHAKE_HEADER_SIZE+8)
#define MAX_HANDSHAKE_HEADER_SIZE DTLS_HANDSHAKE_HEADER_SIZE

/* Record Protocol */
typedef enum content_type_t {
	GNUTLS_CHANGE_CIPHER_SPEC = 20, GNUTLS_ALERT,
	GNUTLS_HANDSHAKE, GNUTLS_APPLICATION_DATA,
	GNUTLS_HEARTBEAT
} content_type_t;

const char *_gnutls_packet2str(content_type_t packet);

int get_errno();
void set_errno(int newerrno);