extern crate bindgen;
extern crate serde;

use std::path::Path;

mod bindgenx;

fn generate_lib(mut config: bindgenx::BindgenConfig, header: &str, generated: &str) {
    // Configure and generate bindings.
    let builder = config
        .build()
        .clang_args(&[
        /*  "-I/usr/powerpc-linux-gnu/include",
            "-I/usr/lib/gcc-cross/powerpc-linux-gnu/5.4.0/include",
         */
            "-v",
        ])
        .header(header)
        .parse_callbacks(Box::new(bindgen::CargoCallbacks));

    println!("bindgen {}", builder.command_line_flags().join(" "));

    let bindings = builder.generate().expect("Unable to generate bindings");

    // Write the generated bindings to an output file.
    bindings
        .write_to_file(generated)
        .expect("Couldn't write bindings!");
}

fn main() {
    let header: &str = "include/gnutls_wrapper.h";
    let c_helper: &str = "src/gnutls_rust.c";
    let library: &str = "gnutls";
    let c_helper_library = "gnutls_rust.a";

    cc::Build::new()
        .file(c_helper)
        .compile(c_helper_library);

    let generated: String = format!("src/{}_bindings.rs", library);
    println!("cargo:rustc-link-lib={}", library);
    println!("cargo:rustc-link-lib={}", c_helper_library);
    println!("cargo:rerun-if-changed={}", header);
    println!("cargo:rerun-if-changed={}", c_helper);


    /*
     std::env::set_var("PKG_CONFIG_SYSROOT_DIR", "/usr/lib/powerpc-linux-gnu");
    let version: &str = "3.7.1";
     //Bad luck with powerpc pkg config in cross
     pkg_config::Config::new()
         .env_metadata(true)
         .print_system_cflags(true)
         .exactly_version(version)
         .statik(true)
         .probe(library)
         .expect("error in pkg_config");
     */
    let config_file = "build-resources/gnutls_bindings.toml";
    println!("cargo:rerun-if-changed={}", config_file);
    let config = bindgenx::BindgenConfig::from_file(Path::new(config_file));




    generate_lib(config, header, generated.as_str())
}
