use gnutls_rs::certificates::{Credentials, CredentialsBuilder, GnutlsSecParams};
use gnutls_rs::cookie_key::CookieKey;
use gnutls_rs::{dtls, Settings};
use std::io::{Error, ErrorKind, Result};
use std::net::SocketAddr;
use std::path::PathBuf;
use std::pin::Pin;
use std::time::Duration;
use tokio::io::{AsyncBufReadExt, BufReader};
use tokio::net::UdpSocket;
use tokio::runtime::Handle;
use tokio::time::sleep;
use udp_listener::UdpStack;
use gnutls_rs::dtls::{DtlsStream, Transport};
use gnutls_sys::gnutls_bindings::{GNUTLS_E_AGAIN, GNUTLS_E_INTERRUPTED, gnutls_global_init};


#[tokio::main]
pub async fn main() {
    env_logger::init();


    unsafe {
        gnutls_global_init();
    }

    gnutls_rs::log::set_log_level(31337);
    gnutls_rs::log::set_log_func(|s| {
        log::trace!("GNUTLS - {}", s);
    });

    let client_addr: &str = "127.0.0.1:31338";
    let server_addr: &str = "127.0.0.1:31337";

    run_client(client_addr, server_addr).await;

    println!("Main is done.");
}

fn parse_addr(addr: &str) -> Result<SocketAddr> {
    let s: SocketAddr = match addr.parse() {
        Ok(s) => s,
        Err(e) => return Err(Error::new(ErrorKind::AddrNotAvailable, e))
    };
    return Ok(s);
}

async fn run_client(
    client_addr: &str,
    server_addr: &str,
) -> std::result::Result<(), Box<dyn std::error::Error + Send + Sync>> {
    sleep(Duration::from_secs(2)).await;

    let gnutls = build_settings("client");

    let stdin = tokio::io::stdin();
    let bufreader = BufReader::new(stdin);
    let mut lines = bufreader.lines();
    println!("run_client - binding to {}", client_addr);
    let socket = UdpSocket::bind(client_addr).await;
    if let Err(e) = socket {
        return Err(Box::new(e));
    }
    let socket = socket.unwrap();

    let stack = UdpStack::new("client",socket, gnutls.get_mtu(), 4096);

    let handle = Handle::current();

    println!("run_client - connecting to {}", server_addr);
    let (udp_stream, join_result) = stack.connect(handle, parse_addr(server_addr)?).await?;

    let mut transport =  Transport::new("client", gnutls.get_mtu(), udp_stream);

    println!("run_client - connect_stream stream");
    let session = match dtls::connect_session(gnutls, transport).await {
        Ok(Some(s)) => s,
        Ok(None) => {
            println!("run_client - connect_session was none");
            return Ok(())
        },
        Err(e) => {
            println!("run_client - connect_stream error {}", e);
            return Err(Box::new(e))
        }
    };

    println!("run_client - starting stream");
    let mut dtls_stream =  Pin::new(Box::new(DtlsStream::new(session)));

    println!("run_client - running loop");
    loop {
        let result =  lines.next_line().await;
        if !result.is_ok() {
            break;
        }
        let line = result.unwrap();
        if line.is_none() {
            break;
        }
        let line = line.unwrap();

        while let Ok(_) = dtls_stream.send(&line.as_bytes()) {
            if let Ok(decrypted) = dtls_stream.recv() {
                if let Ok(str) = String::from_utf8(decrypted) {
                    println!("Received: {}", str);
                }
            }
        }
    }

    join_result.join().await;

    Ok(())
}


fn build_settings(_: &str) -> Settings {
    //../x509-ca-key.pem  ../x509-ca.pem  ../x509-client.p12  ../x509-server-key.pem  ../x509-server.pem
    let ca_file = "target/ca-cert.pem";
    let cert_file = "target/cert.pem";
    let key_file = "target/key.pem";
    let x509: Credentials = CredentialsBuilder::new()
        .ca_file(PathBuf::from(ca_file))
        //.crl_file(PathBuf::from(crl_file))
        .key_files(PathBuf::from(key_file), PathBuf::from(cert_file))
        .sec_params(GnutlsSecParams::GnutlsSecParamMedium)
        .build()
        .expect("Could not create credentials");


    let mtu = 1400;

    //generate cookie key
    let cookie: CookieKey = CookieKey::generate().expect("Could not generate cookie");

    gnutls_rs::Settings::new(x509,  cookie, mtu)
}
