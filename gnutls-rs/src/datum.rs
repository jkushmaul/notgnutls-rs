use core::mem;
use std::os::raw::c_uint;
use gnutls_sys::gnutls_bindings::gnutls_datum_t;

#[derive(Debug, Clone)]
pub struct GnutlsDatum {
    vector: Vec<u8>,
}

impl GnutlsDatum {
    fn new(vector: Vec<u8>) -> Self {
        Self { vector }
    }

    pub fn as_raw(&mut self) -> gnutls_datum_t {
        return gnutls_datum_t { data: self.vector.as_mut_ptr(), size: self.vector.capacity() as c_uint };
    }

    pub fn from_raw(datum: gnutls_datum_t) -> Self {
        unsafe {
            let v: Vec<u8> = Vec::from_raw_parts(datum.data, datum.size as usize, datum.size as usize);
            GnutlsDatum::new(v)
        }
    }
    pub fn new_raw() -> gnutls_datum_t {
        unsafe {
            let datum: gnutls_datum_t = mem::zeroed();
            return datum;
        }
    }
}
impl From<Vec<u8>> for GnutlsDatum {
    fn from(v: Vec<u8>) -> Self {
        GnutlsDatum::new(v)
    }
}
