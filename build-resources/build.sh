#!/bin/bash
set -e
cd "$(dirname "$0")" && (
  docker build -t gnutls-rs.cross-powerpc ./ -f gnutls-rs.cross-powerpc.Dockerfile
  docker build -t gnutls-rs.cross-x86_64 ./ -f gnutls-rs.cross-x86_64.Dockerfile
)
cross build --target=powerpc-unknown-linux-gnu
cross build --target=x86_64-unknown-linux-gnu

