use serde::Deserialize;
use std::path::Path;

#[derive(Deserialize)]
pub struct BindgenConfig {
    style: BindgenStyleConfig,
    import: BindgenImportConfig,
}

#[derive(Deserialize)]
struct BindgenImportConfig {
    functions: Vec<String>,
    types: Vec<String>,
    vars: Vec<String>,
    constified_enum_modules: Vec<String>,
    bitfield_enums: Vec<String>,
}

#[derive(Deserialize)]
struct BindgenStyleConfig {
    raw_lines: Vec<String>,
}

impl BindgenStyleConfig {
    fn build(&mut self, mut builder: bindgen::Builder) -> bindgen::Builder {
        for l in self.raw_lines.iter() {
            builder = builder.raw_line(l);
        }
        return builder;
    }
}

impl BindgenImportConfig {
    fn build(&mut self, mut builder: bindgen::Builder) -> bindgen::Builder {
        for e in self.constified_enum_modules.iter() {
            builder = builder.constified_enum_module(e);
        }
        for b in self.bitfield_enums.iter() {
            builder = builder.bitfield_enum(b);
        }
        for v in self.vars.iter() {
            builder = builder.allowlist_var(v);
        }
        for t in self.types.iter() {
            builder = builder.allowlist_type(t);
        }
        for f in self.functions.iter() {
            builder = builder.allowlist_function(f);
        }
        builder = builder
            .generate_comments(true)
            .fit_macro_constants(false)
            .size_t_is_usize(true)
            .layout_tests(true);

        return builder;
    }
}

impl BindgenConfig {
    pub fn from_file(file_path: &Path) -> BindgenConfig {
        let file_path_str = file_path.to_str().expect("Could not convert Path to str");
        let toml_str = std::fs::read_to_string(file_path)
            .expect(format!("Could not read from config file {}", file_path_str).as_str());
        return toml::from_str(toml_str.as_str())
            .expect(format!("File in wrong format {}", file_path_str).as_str());
    }
    pub fn build(&mut self) -> bindgen::Builder {
        let mut builder = bindgen::builder();
        builder = self.import.build(builder);
        builder = self.style.build(builder);
        return builder;
    }
}
