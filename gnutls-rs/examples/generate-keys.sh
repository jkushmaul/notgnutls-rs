set -e


OUTPUTDIR="$1"
test -d "$1" || { echo "usage: $0 <outputdir>"; exit 1; }
TEMPLATEDIR="$(dirname "$(readlink -f "$0")")"


cargo build --examples

certtool --generate-privkey --outfile  "$OUTPUTDIR/ca-key.pem"
certtool --generate-self-signed --load-privkey  "$OUTPUTDIR/ca-key.pem" --outfile  "$OUTPUTDIR/ca-cert.pem" --template "$TEMPLATEDIR/ca.template"
certtool --generate-privkey --outfile   "$OUTPUTDIR/key.pem"
certtool --generate-certificate --load-privkey  "$OUTPUTDIR/key.pem" --outfile  "$OUTPUTDIR/cert.pem" --load-ca-certificate "$OUTPUTDIR/ca-cert.pem" --load-ca-privkey  "$OUTPUTDIR/ca-key.pem"  --template "$TEMPLATEDIR/cert.template"

echo "$OUTPUTDIR/ca-key.pem"
echo "$OUTPUTDIR/ca-cert.pem"
echo "$OUTPUTDIR/key.pem"
echo "$OUTPUTDIR/cert.pem"



#
