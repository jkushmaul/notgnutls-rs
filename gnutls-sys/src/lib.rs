extern crate core;
extern crate foreign_types;

use foreign_types::foreign_type;

pub mod gnutls_bindings;


//unsafe impl Send for gnutls_bindings::gnutls_session_int {}


foreign_type! {
    /*/// Documentation for the owned type.
    pub unsafe type Ssl: Sync + Send {
        type CType = gnutls_bindings::gnutls_session_t;
        fn drop = openssl_sys::SSL_free;
        fn clone = openssl_sys::SSL_dup;
    }*/

    /// This type immutably borrows other data and has a limited lifetime!
    pub unsafe type GnutslSessionPtr<'a>: Send {
        type CType = gnutls_bindings::gnutls_session_t;
        type PhantomData = &'a ();
        fn drop = gnutls_bindings::gnutls_deinit;
    }
}