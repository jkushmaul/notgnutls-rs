use crate::datum::GnutlsDatum;
use crate::dtls::DtlsPrestate;
use gnutls_sys::gnutls_bindings::{gnutls_dtls_cookie_verify, gnutls_key_generate, GNUTLS_COOKIE_KEY_SIZE, MAX_HANDSHAKE_HEADER_SIZE, DTLS_MAX_COOKIE_SIZE, gnutls_datum_t, gnutls_dtls_cookie_send, gnutls_transport_ptr_t, GNUTLS_E_LARGE_PACKET};
use std::net::{SocketAddr};
use std::io::{Error, ErrorKind};
use std::os::raw::c_void;
use crate::ffi::{Ffi, FfiConversion};

#[derive(Debug, Clone)]
pub struct CookieKey {
    datum: GnutlsDatum,
}

#[repr(C)]
#[derive(Debug, Clone)]
pub struct ClientData {
    octets: [u8; 18],
    len: usize,
}
impl ClientData {
    pub fn len(&self) -> usize {
        return self.len;
    }
    pub fn as_cvoid(&mut self) -> *mut c_void {
        return self.octets.as_mut_ptr() as *mut c_void;
    }
    pub fn new(sa_addr: &SocketAddr) -> Self {
        let mut octets = [0u8; 18];
        let h_port: u16 = sa_addr.port() / 0xff;
        let l_port: u16 = sa_addr.port() - h_port;
        octets[0] = h_port as u8;
        octets[1] = l_port as u8;

        match sa_addr {
            SocketAddr::V4(v4) => {
                octets[2..6].copy_from_slice(&v4.ip().octets());
                Self {octets, len: 6}
            }
            SocketAddr::V6(v6) => {
                octets[2..].copy_from_slice(&v6.ip().octets());
                Self {octets, len: 6}
            }
        }
    }
}


impl CookieKey {
    pub fn generate() -> Result<CookieKey, i32> {
        unsafe {
            let mut datum: gnutls_datum_t = GnutlsDatum::new_raw();
            let result = gnutls_key_generate(&mut datum, GNUTLS_COOKIE_KEY_SIZE);
            if result != 0 {
                log::debug!("Error creating cookie: {}", result);
                return Err(-1);
            }
            let datum = GnutlsDatum::from_raw(datum);
            Ok(Self {datum})
        }

    }

    pub fn verify(&mut self, remote_addr: &SocketAddr, msg: &[u8]) -> std::io::Result<Option<DtlsPrestate>> {
        let mut prestate = DtlsPrestate::new();
        let mut client_data = ClientData::new(remote_addr);

        log::debug!("verify_prestate() against {} bytes", msg.len());
        unsafe {
            let result = gnutls_dtls_cookie_verify(
                 &mut self.datum.as_raw(),
                client_data.as_cvoid(),
                client_data.len(),
                msg.as_ptr() as *mut c_void,
                msg.len(),
                &mut prestate.prestate,
            );
            if result < 0 {
                log::debug!("gnutls_dtls_cookie_verify returned error {}", result);
                return Ok(None);
            }
            return Ok(Some(prestate));
        }
    }

    pub fn send(&mut self, remote_addr: &SocketAddr) -> std::io::Result<Vec<u8>> {
        const MAX_HVR_LEN: usize = (20 + MAX_HANDSHAKE_HEADER_SIZE + DTLS_MAX_COOKIE_SIZE) as usize;
        let mut prestate = DtlsPrestate::new();
        let mut client_data = ClientData::new(remote_addr);
        let mut send_buffer_box = Box::new(vec![0u8; MAX_HVR_LEN]);
        let result: i32;
        unsafe {

            result = gnutls_dtls_cookie_send(
                &mut self.datum.as_raw(),
                client_data.as_cvoid(),
                client_data.len(),
                &mut prestate.prestate,
                Ffi::as_cvoid(&mut send_buffer_box),
                Some(gnutls_stupid_copy_buffer_cb));
            if result <= 0 {
                return Err(Error::new(ErrorKind::Other, format!("Could not send cookie data: {}", result)));
            }

            let len: usize = result as usize;
            log::debug!("send_buffer with len {}", len);
            let mut send_buffer = *send_buffer_box;
            send_buffer.set_len(len);

            Ok(Vec::from(&send_buffer[..len]))
        }
    }
}

#[no_mangle]
unsafe extern "C" fn gnutls_stupid_copy_buffer_cb(
    tx_ptr: gnutls_transport_ptr_t,
    hvr: *const ::std::os::raw::c_void,
    hvr_len: usize,
) -> isize {
    let tx = Ffi::from_cvoid(tx_ptr);

    match  FfiConversion::copy_from_cvoid(*tx, hvr, hvr_len) {
        Ok(_) => hvr_len as isize,
        Err(_) => return GNUTLS_E_LARGE_PACKET as isize
    }
}