use core::{mem, ptr};
use gnutls_sys::gnutls_bindings::{gnutls_priority_deinit, gnutls_priority_init2, gnutls_priority_t, GNUTLS_PRIORITY_INIT_DEF_APPEND, GNUTLS_E_SUCCESS};
use std::ffi::CString;
use std::os::raw::c_int;

pub struct PriorityBuilder {
    priorities: Option<String>,
    append: bool,
}

#[derive(Debug, Clone)]
pub struct Priority {
    priority_cache: Option<gnutls_priority_t>,
}
impl Priority {
    pub fn get_priority_cache(&self) -> Option<gnutls_priority_t> {
        return self.priority_cache;
    }
}

impl PriorityBuilder {
    pub fn new() -> Self {
        let priorities = None;
        let append: bool = false;
        return Self { priorities, append };
    }

    pub fn append(mut self, append: bool) -> Self {
        self.append = append;
        return self;
    }

    pub fn priorities(mut self, priorities: &str) -> Self {
        self.priorities = Some(String::from(priorities));
        return self;
    }

    pub fn build(self) -> Result<Priority, i32> {
        let flags: u32   = GNUTLS_PRIORITY_INIT_DEF_APPEND;

        let mut priority_cache: Option<gnutls_priority_t> = None;
        unsafe {
            let mut pc: gnutls_priority_t = mem::zeroed();

            if let Some(priorities) = self.priorities {
                let priorities = CString::new(priorities).unwrap();

                let err_pos: *const u8 = ptr::null_mut();
                let err = gnutls_priority_init2(
                    &mut pc,
                    priorities.as_ptr(),
                    err_pos as *mut *const std::os::raw::c_char,
                    flags,
                );

                if err != GNUTLS_E_SUCCESS as c_int {
                    log::debug!("::build() - error returned from gnutls_priority_init2: {}", err);
                    return Err(err);
                }
                priority_cache = Some(pc);
            }
            return Ok( Priority { priority_cache } );
        }

    }
}

impl Priority {

}

impl Drop for Priority {
    fn drop(&mut self) {
        if let Some(priority_cache) = self.priority_cache {
            unsafe {
                gnutls_priority_deinit(priority_cache);
                self.priority_cache = None;
            }
        }
    }
}
