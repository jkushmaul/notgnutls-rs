use std::io::{Error, ErrorKind};

use std::os::raw::c_void;


pub struct FfiConversion {}


pub struct Ffi {}
impl Ffi {
    pub fn as_cvoid<T>(obj: &mut Box<T>) -> *mut c_void {
        &mut *obj as *mut _ as *mut c_void
    }
    pub fn from_cvoid<T>(ptr: *mut c_void) -> *mut &'static mut T {
        ptr as *mut &mut T
    }
    pub fn as_ptr<T>(obj: &mut Box<T>) -> &mut T {
        &mut *obj
    }
}



impl FfiConversion {
    #[allow(dead_code)]
    pub(crate) fn new_from_cvoid(data: *const c_void, data_len: usize) -> std::io::Result<Vec<u8>> {
        let mut v: Vec<u8> = Vec::with_capacity(data_len);
        FfiConversion::copy_from_cvoid(&mut v, data, data_len)?;
        return Ok(v);
    }

    pub fn copy_from_cvoid(v: &mut Vec<u8>, data: *const c_void, data_len: usize) -> std::io::Result<()>{
        if v.capacity() < data_len {
            println!("FfiConversion::copy_from_cvoid - capacity({}) was < {}", v.capacity(), data_len);
            return Err(Error::new(ErrorKind::Other, "Buffer was not large enough"));
        }

        unsafe {
            v.as_mut_ptr().copy_from(data as *const u8, data_len);
            v.set_len(data_len);
        }
        Ok(())
    }

    #[allow(dead_code)]
    pub(crate) fn copy_to_cvoid(v: &Vec<u8>, data: *const c_void, data_len: usize) -> std::io::Result<()> {
        if data_len < v.len() {
            return Err(Error::new(ErrorKind::Other, "Buffer was not large enough"));
        }
        unsafe {
            log::debug!("About to copy {} bytes into *void buffer with {} bytes", v.len(), data_len);
            std::ptr::copy(v.as_ptr(),  data as *mut u8, v.len());
        }
        Ok(())
    }
}
