use crate::certificates::Credentials;
use crate::dtls::{DtlsPrestate, Transport};
use crate::with_int_result;
use core::mem;
use std::ffi::{CString};
use gnutls_sys::gnutls_bindings::gnutls_close_request_t::GNUTLS_SHUT_WR;
use gnutls_sys::gnutls_bindings::{gnutls_bye, gnutls_certificate_credentials_t, gnutls_credentials_set, gnutls_dtls_prestate_set, gnutls_dtls_prestate_st, gnutls_dtls_set_mtu, gnutls_dtls_set_timeouts, gnutls_handshake, gnutls_init_flags_t, gnutls_server_name_set, gnutls_session_t, gnutls_set_default_priority, gnutls_set_default_priority_append, gnutls_transport_set_int2, gnutls_transport_set_ptr, gnutls_transport_set_pull_function, gnutls_transport_set_pull_timeout_function, gnutls_transport_set_push_function};
use gnutls_sys::gnutls_bindings::{
    gnutls_deinit, gnutls_init,
};
use std::io::{Error, ErrorKind};
use std::os::unix::io::{RawFd};
use std::pin::Pin;
use libc::c_void;
use gnutls_sys::gnutls_bindings::gnutls_credentials_type_t::GNUTLS_CRD_CERTIFICATE;
use gnutls_sys::gnutls_bindings::gnutls_server_name_type_t::GNUTLS_NAME_DNS;

pub struct SessionBuilder {
    name: String,
    credentials: Option<gnutls_certificate_credentials_t>,
    flags: Option<gnutls_init_flags_t>,
    server_name: Option<String>,
    dtls_mtu: Option<usize>,
    dtls_timeout: Option<(u32, u32)>,
    prestate: Option<DtlsPrestate>,
    priority: Option<String>,
    raw_fd: Option<RawFd>,
}
impl SessionBuilder {
    pub fn new(name: &str) -> Self {
        let name = name.to_string();
        return Self {
            name,
            credentials: None,
            flags: None,
            server_name: None,
            priority: None,
            dtls_mtu: None,
            dtls_timeout: None,
            prestate: None,
            raw_fd: None,
        };
    }

    pub fn raw_fd(&mut self, raw_fd: RawFd) -> &mut Self {
        self.raw_fd = Some(raw_fd);
        return self;
    }

    pub fn server_name(&mut self, server_name: String) -> &mut Self {
        self.server_name = Some(server_name.clone());
        return self;
    }

    pub fn flags(&mut self, flags: gnutls_init_flags_t) -> &mut Self  {
        self.flags = Some(flags);
        return self;
    }

    pub fn credentials(&mut self, credentials: &Credentials) -> &mut Self {
        self.credentials = Some(credentials.get_xcred());
        return self;
    }

    pub fn priority(&mut self, priority: &str) -> &mut Self {
        self.priority = Some(priority.to_string());
        return self;
    }

    pub fn dtls_mtu(&mut self, mtu: usize) -> &mut Self {
        self.dtls_mtu = Some(mtu);
        return self;
    }

    pub fn dtls_timeouts(&mut self, retrans_timeout: u32, total_timeout: u32) -> &mut Self {
        self.dtls_timeout = Some((retrans_timeout, total_timeout));
        return self;
    }

    pub fn dtls_prestate(&mut self, prestate: DtlsPrestate) -> &mut Self {
        self.prestate = Some(prestate);
        return self;
    }


    pub fn build(self, transport: Transport) -> std::io::Result<Session> {
        if self.flags.is_none() {
            return Err(Error::new(ErrorKind::Other, "At least 1 flag is required"));
        }
        let flags = self.flags.unwrap();
        let mut session: Session = match Session::new(self.name, flags, transport) {
            Ok(s) => s,
            Err(e) => return Err(Error::new(ErrorKind::Other, format!("Error creating session: {}", e)))
        };

        if let Some(c) = self.credentials {
            session
                .set_credentials(c)
                .unwrap();
        }

        if let Some(prestate) = self.prestate {
            session.set_dtls_prestate(prestate.prestate);
        }

        if let Err(e) = session.set_priority(self.priority) {
            return Err(Error::new(ErrorKind::InvalidData, format!("Could not set priority: {}", e)));
        }

        if self.server_name.is_some() {
            let server_name = self.server_name.unwrap();
            log::debug!("setting servername: {}", server_name);
            if let Err(e) = session.set_server_name(server_name.as_str()) {
                return Err(Error::new(ErrorKind::InvalidData, format!("Could not set server name: {}", e)));
            }
        }

        if self.dtls_mtu.is_some() {
            session.set_dtls_mtu(self.dtls_mtu.unwrap() as u32);
        }
        if self.dtls_timeout.is_some() {
            let (retrans_timeout, total_timeout) = self.dtls_timeout.unwrap();
            session.set_dtls_timeouts(retrans_timeout, total_timeout);
        }
        if self.raw_fd.is_some() {
            let fd: i32 = self.raw_fd.unwrap();
            session.set_raw_fd(fd);
        }



        return Ok(session);
    }
}

//Transport contains Receiver, may not be cloned
#[derive(Debug)]
pub struct Session {
    name: String,
    transport: Pin<Box<Transport>>,
    session: Pin<Box<gnutls_session_t>>,
}

impl Session {
    fn new(name: String, flags: gnutls_init_flags_t, transport: Transport) -> Result<Self, i32> {

        unsafe {
            let mut session: gnutls_session_t = mem::zeroed();
            with_int_result(|| {
                return gnutls_init(&mut session, flags.0);
            })?;

            let mut s = Self { name, session: Pin::new(Box::new(session)), transport: Pin::new(Box::new(transport))};

            s.assign_transport();


            Ok(s)
        }
    }

     fn assign_transport(&mut self) {
        let tx_ptr = self.transport.to_cvoid();
        unsafe {
            gnutls_transport_set_ptr(*self.get_session(), tx_ptr);
            gnutls_transport_set_pull_function(*self.get_session(), Some(Transport::udp_stream_pull_func));
            gnutls_transport_set_push_function(*self.get_session(), Some(Transport::udp_stream_push_func));
            gnutls_transport_set_pull_timeout_function(*self.get_session(), Some(Transport::udp_stream_pull_timeout_func));
        }
    }

    pub(crate) fn get_transport(&self) -> &Transport {
        return &self.transport;
    }
    pub(crate) fn get_session(&mut self) -> &mut gnutls_session_t {
        return self.session.as_mut().get_mut()
    }
    pub(crate) fn take_session(mut self) -> Pin<Box<gnutls_session_t>> {
        return self.session;
    }

    fn set_priority(&mut self, priority: Option<String>) -> Result<(), i32> {
        if let Some(priority) = priority {
            let priority = match CString::new(priority) {
                Ok(s) => s,
                Err(_) => return Err(-1),
            };
            with_int_result(|| unsafe {
                return gnutls_set_default_priority_append(*self.get_session(), priority.as_ptr(), std::ptr::null_mut(), 0);
            })?;
        } else {
            with_int_result(|| unsafe {
                return gnutls_set_default_priority(*self.get_session());
            })?;
        }


        return Ok(());
    }

    fn set_dtls_prestate(&mut self, mut prestate: gnutls_dtls_prestate_st) {
        unsafe {
            gnutls_dtls_prestate_set(*self.get_session(), &mut prestate);
        }
    }

    fn set_dtls_mtu(&mut self, mtu: u32) {
        unsafe {
            gnutls_dtls_set_mtu(*self.get_session(), mtu);
        }
    }

    fn set_dtls_timeouts(&mut self, retrans_timeout: u32, total_timeout: u32) {
        unsafe {
            gnutls_dtls_set_timeouts(*self.get_session(), retrans_timeout, total_timeout);
        }
    }

    fn set_credentials(&mut self, cred: gnutls_certificate_credentials_t) -> Result<i32, i32> {

        return with_int_result(|| unsafe {
            return gnutls_credentials_set(
                *self.get_session(),
                GNUTLS_CRD_CERTIFICATE,
                mem::transmute(cred),
            );
        });
    }

    fn set_raw_fd(&mut self, raw_fd: RawFd) {
         unsafe {
             gnutls_transport_set_int2(*self.get_session(), raw_fd, raw_fd);
         }
    }

    fn set_server_name(&mut self, server_name: &str) -> Result<i32, i32> {
        let data = server_name.as_ptr();
        let len = server_name.len();

        return with_int_result(|| unsafe {
            return gnutls_server_name_set(
                *self.get_session(),
                GNUTLS_NAME_DNS,
                data as *mut c_void,
                len,
            );
        });
    }

    pub(crate) fn handshake(&mut self) -> Result<i32, i32> {
        unsafe {
            let result = gnutls_handshake(*self.get_session());
            if result < 0 {
                return Err(result);
            }
            return Ok(result);
        }
    }



    pub fn bye(&mut self) {
        log::debug!("{} saying bye", self.name);
        unsafe {
            gnutls_bye(*self.get_session(), GNUTLS_SHUT_WR);
        }
    }


}

impl Drop for Session {
    fn drop(&mut self) {
        log::debug!("{} dropping session", self.name);
        unsafe {
            //I'm afraid to call bye() here...
            gnutls_deinit(*self.get_session());
        }
    }
}

