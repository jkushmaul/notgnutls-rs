use core::slice;
use std::os::raw::c_void;
use tokio::sync::mpsc::{Receiver, Sender};
use gnutls_sys::gnutls_bindings::{GNUTLS_E_AGAIN, GNUTLS_E_INTERRUPTED, gnutls_record_recv, gnutls_record_send, gnutls_session_t};
use crate::session::Session;
use std::io::{Error, ErrorKind, Result};
use std::mem;
use std::ops::{Deref, DerefMut};
use std::pin::Pin;
use std::sync::{Arc, LockResult};
use std::time::Duration;
use tokio::net::UdpSocket;
use tokio::runtime::Handle;
use tokio::sync::Mutex;
use tokio::task;
use tokio::task::JoinHandle;
use crate::dtls::Transport;

struct GnutlsStack {
    session: Session,
    egress_rx: Receiver<Vec<u8>>,
    ingress_tx: Sender<Vec<u8>>
}

struct TestStack {
}

impl TestStack {
    pub async fn test_send(){
        let mut v = vec![0u8; 4096];
        let session: gnutls_session_t = unsafe { mem::zeroed() };
        let session = Arc::new(Mutex::new(Box::new(session)));

        let result =  tokio::spawn( async move {
            tokio::time::sleep(Duration::from_micros(100)).await;
            let locked_session = session.lock().await;
            {
                let boxed_session = *locked_session;
                let sz: i32 = unsafe {
                    gnutls_record_recv(
                        *boxed_session,
                        v.as_mut_ptr() as *mut c_void,
                        v.capacity(),
                    ) as i32
                };
            }
        });
        result.await;
    }
}

impl GnutlsStack {     /*
    pub async fn forward2(mut stack: GnutlsStack, handle: Handle) -> Result<JoinHandle<Result<()>>> {

              let udp_socket: UdpSocket = UdpSocket::bind("127").await?;
              let stack = stack;
              let mut session = stack.session;
              let ingress_tx = stack.ingress_tx;
              let mut egress_rx = stack.egress_rx;
              let transport = session.get_transport();
              let mtu = transport.get_mtu();
              let session = session.take_session();

              let result = handle.spawn( async move {

                  let egress_data: Option<Vec<u8>> = None;
                  let ingress_data: Option<usize> = None;
                  let buff = vec![0u8; mtu];
                  GnutlsStack::egress(&session, buff);


                let mut buffer = vec![0u8; mtu];

                loop {
                    let egress_data: Option<Vec<u8>> = None;
                    let ingress_data: Option<usize> = None;
                    let buff = vec![0u8; mtu];
                    GnutlsStack::egress(&session, buff);

                    tokio::select! {
                        egress_result = egress_rx.recv() => {
                            match egress_result {
                                Some(v) => {
                                    egress_data = Some(v);
                                },
                                None => break,
                            }
                        },
                        ingress_result = udp_socket.recv(&mut buffer) => {
                            match ingress_result {
                                Ok(v) => {
                                    ingress_data = Some(v);
                                },
                                Err(e) => return Err(e)
                            }
                        }
                    };

                    if egress_data.is_some() {
                        GnutlsStack::egress(&session, egress_data.unwrap());
                    }

                    if ingress_data.is_some() {
                        GnutlsStack::ingress(&session, &ingress_tx, &mut buffer).await;
                    }

                  }
            let result: std::io::Result<()> = Ok(());
            return result;
        });

        return Ok(result);
    }*/


    fn egress(session: &Pin<Box<gnutls_session_t>>, data: Vec<u8>) -> Result<()> {
        log::debug!("gnutls_record_send() for {} bytes", data.len());
        let ret = tokio::task::block_in_place(|| {
            let sz = unsafe {
                gnutls_record_send(
                    **session,
                    data.as_ptr() as *mut c_void,
                    data.len(),
                )
            };
            return sz;
        });
        if ret < 0 {
            return Err(Error::new(ErrorKind::Other, format!("Error in gnutls_record_send: {}", ret)));
        }
        Ok(())
    }

    async fn ingress(session:  &Pin<Box<gnutls_session_t>>, ingress_tx: &Sender<Vec<u8>>, buffer: &mut Vec<u8>) -> Result<()> {
        let data_len = buffer.capacity();
        let data = buffer.as_mut_ptr();

        let mut ret = GNUTLS_E_INTERRUPTED;

        loop {
            ret = tokio::task::block_in_place(|| {
                unsafe {
                    let sz: i32 = gnutls_record_recv(
                        **session,
                        data as *mut c_void,
                        data_len,
                    ) as i32;
                    return sz;
                }
            });
            if ret == GNUTLS_E_INTERRUPTED {
                tokio::time::sleep(Duration::from_micros(100)).await;
                continue;
            }
            break;
        }

        if ret < 0 {
            return Err(Error::new(ErrorKind::Other, format!("Error in gnutls_record_recv: {}", ret)));
        };
        if ret as usize > data_len {
            return Err(Error::new(ErrorKind::Other, format!("Data ready was too large: {}", ret)));
        }

        //because length was not set, rather than set length unsafely, just from_raw_parts unsafely.

        let s = unsafe {
            slice::from_raw_parts(data, ret as usize)
        };

        let v = s.to_vec();
        ingress_tx.send(v).await;
        Ok(())
    }

    /*
     pub async fn forward(mut self, handle: Handle)   {



         let mtu = self.session.get_transport().get_mtu();
         let session = self.session.get_session();
         let ingress_tx = self.ingress_tx;
         let egress_rx = self.egress_rx;



         let ingress_join: JoinHandle<std::io::Result<()>> = handle.spawn_blocking( move || {
             GnutlsStack::ingress( handle.clone(), session, ingress_tx, mtu)
         });
         let egress_join: JoinHandle<std::io::Result<()>> = handle.spawn(async move {
             GnutlsStack::egress(handle, session, egress_rx).await
         });
         let (ingress_result, egress_result) = tokio::join!(ingress_join, egress_join);


    }

    fn ingress(handle: Handle, session: &mut gnutls_session_t, ingress_tx: Sender<Vec<u8>>, mtu: usize) -> Result<()> {
        let mut buffer: Vec<u8> = vec![0u8; mtu];
        let data_len = buffer.capacity();
        let data = buffer.as_mut_ptr();

        //instead, this should recv from channel, the stack will forward it through.
        loop {
            unsafe {
                //this will call pull func

                let ret = gnutls_record_recv(
                    *session,
                    data as *mut c_void,
                    data_len,
                );

                if ret < 0 {
                    let ret = ret as i32;
                    if ret == GNUTLS_E_AGAIN || ret == GNUTLS_E_INTERRUPTED {
                        continue;
                    }
                    return Err(Error::new(ErrorKind::Other, format!("Error in gnutls_record_recv: {}", ret)));
                };
                if ret as usize > data_len {
                    return Err(Error::new(ErrorKind::Other, format!("Data ready was too large: {}", ret)));
                }
                //because length was not set, rather than set length unsafely, just from_raw_parts unsafely.
                let s = slice::from_raw_parts(data, ret as usize);
                let v = s.to_vec();
                handle.block_on( async move {
                    ingress_tx.send(v).await;
                });

            }
        }
        Ok(())
    }

    async fn egress(handle: Handle, session: &mut gnutls_session_t, mut egress_rx: Receiver<Vec<u8>>) -> Result<()> {
        while let Some(data) = egress_rx.recv().await {
            unsafe {
                //this will call push func
                log::debug!("gnutls_record_send() for {} bytes", data.len());
                let ret = tokio::task::block_in_place(  || {gnutls_record_send(
                    *session,
                    data.as_ptr() as *mut c_void,
                    data.len(),
                )});
                if ret < 0 {
                    return Err(Error::new(ErrorKind::Other, format!("Error in gnutls_record_send: {}", ret)));
                }
            }
        }
        Ok(())
    }
    */
}