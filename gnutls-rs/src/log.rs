use std::ffi::CStr;
use gnutls_sys::gnutls_bindings::{gnutls_audit_log_func, gnutls_global_set_audit_log_function, gnutls_global_set_log_level, gnutls_session_t};


static mut LOG_FUNC: fn(&str) = internal_log;


fn internal_log(msg: &str) {
    log::debug!("{}", msg);
}

pub fn set_log_level(level: i32) {
    unsafe {
        gnutls_global_set_log_level(level);
    }
}

pub fn set_log_func(func: fn(&str)) {

    let wrapper : gnutls_audit_log_func =  Some(wrapper_log_func);
    unsafe {
        LOG_FUNC = func;
        gnutls_global_set_audit_log_function(wrapper );
    }
}



#[no_mangle]
extern "C" fn wrapper_log_func(_: gnutls_session_t, arg2: *const ::std::os::raw::c_char)  {
    let s = unsafe { CStr::from_ptr(arg2) };
    match s.to_str() {
        Ok(s) => {
            unsafe {
                LOG_FUNC(s);
            }
        }
        Err(_) => {}
    }
}

/*
pub fn packet_to_string(packet: &[u8]) -> String {
    unsafe {
        let result = _gnutls_packet2str(packet.as_ptr());


    }
}
*/