FROM rustembedded/cross:powerpc-unknown-linux-gnu

RUN apt-get update && apt-get install -y software-properties-common wget  apt-transport-https ca-certificates
RUN     wget -O - http://apt.llvm.org/llvm-snapshot.gpg.key | apt-key add - && \
        apt-add-repository "deb http://apt.llvm.org/xenial/ llvm-toolchain-xenial-6.0 main" && \
        apt-get update && \
        apt-get install -y clang-6.0 libclang-6.0-dev
RUN  ln -s /usr/bin/clang-6.0 /usr/bin/clang && clang --version | grep "6.0.1" || (echo "clang not 6.0.1"; exit 1)

RUN cd /tmp && wget -q 'https://www.gnupg.org/ftp/gcrypt/gnutls/v3.7/gnutls-3.7.1.tar.xz' -O "gnutls-3.7.1.tar.xz" && \
        tar -xf gnutls-3.7.1.tar.xz && rm -f gnutls-3.7.1.tar.xz && \
        mkdir -p /usr/src && mv gnutls-3.7.1 /usr/src

RUN cd /tmp && wget -q 'https://ftp.gnu.org/gnu/nettle/nettle-3.7.tar.gz' -O "nettle-3.7.tar.gz" && \
        tar -xf nettle-3.7.tar.gz && rm -f nettle-3.7.tar.gz && \
        mv nettle-3.7 /usr/src

RUN cd /usr/src/nettle-3.7 && \
        ./configure --prefix=/usr/ \
            --enable-mini-gmp --disable-documentation && \
        make && make install

RUN cd /usr/src/gnutls-3.7.1 && \
        ./configure --prefix=/usr/\
            --disable-gcc-warnings --disable-doc --disable-guile --without-p11-kit \
            --with-nettle-mini --with-included-libtasn1 --with-included-unistring \
            --enable-static && \
                LDFLAGS='-Wl,-Bsymbolic-functions -Wl,-z,relro -Wl,-z,now' && \
        make && make check TESTS="" && make install