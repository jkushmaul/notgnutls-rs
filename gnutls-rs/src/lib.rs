extern crate gnutls_sys;
extern crate core;
pub mod certificates;
pub mod cookie_key;
pub mod datum;
pub mod dtls;
pub mod priority;
pub mod session;
pub mod ffi;
pub mod log;
mod stack;

use std::ffi::{CStr};
use gnutls_sys::gnutls_bindings::{GNUTLS_E_AGAIN, GNUTLS_E_INTERRUPTED, GNUTLS_E_SUCCESS, gnutls_strerror};
use crate::certificates::Credentials;
use crate::cookie_key::CookieKey;
pub use gnutls_sys::gnutls_bindings::gnutls_init_flags_t;

pub(crate) fn with_int_result<T>(mut func: T) -> Result<i32, i32>
where
    T: FnMut() -> i32,
{
    let ret = func();

    if ret == GNUTLS_E_SUCCESS as i32 {
        return Ok(ret);
    } else if ret != GNUTLS_E_AGAIN && ret != GNUTLS_E_INTERRUPTED {
        unsafe {
            let str = gnutls_strerror(ret);

            if let Ok(cstr) =  CStr::from_ptr(str).to_str() {
                println!("with_int_result in error: {} - {}", ret, cstr);
            } else {
                println!("with_int_result in error: {} - unknown", ret);
            }

        }
    }

    return Err(ret);
}

#[derive(Debug, Clone)]
pub struct Settings {
    x509: Credentials,
    cookie_key: CookieKey,
    mtu: usize,
}

impl Settings {
    pub fn get_mtu(&self) -> usize {
        self.mtu
    }

    pub fn new(
        x509: Credentials,
        cookie_key: CookieKey,
        mtu: usize,
    ) -> Self {
        Self {
            x509,
            cookie_key,
            mtu,
        }
    }

}

